# Recoil

<p align="center">
  <img src="https://gitlab.com/pascal.cantaluppi/recoil/-/raw/master/public/img/recoil_logo.png" alt="Recoil" /></a>
</p>

<p><b>Recoil State Management by Google - Test App (ToDo-List)</b></p>

<p>Recoil is an experimental set of utilities for state management with React.
<br />Please see the website: <a href="https://recoiljs.org">https://recoiljs.org</a></p>

<p align="center">
  <img src="https://gitlab.com/pascal.cantaluppi/recoil/-/raw/master/public/img/recoil_scheme.png" alt="Recoil" /></a>
</p>

// https://www.youtube.com/watch?v=BchtCWxs7sA

import React, { useEffect } from "react";
import { atom, useRecoilState } from "recoil";

const reposState = atom({
  key: "repos",
  default: [],
});

function App() {
  const [repos, setRepos] = useRecoilState(reposState);

  useEffect(() => {
    const getRepos = async () => {
      const url = `https://ghapi.huchen.dev/repositories?since=monthly`;
      const resp = await fetch(url);
      const body = await resp.json();
      console.log(body);
      setRepos(body);
    };
    getRepos();
  }, []);

  return repos.map((repo) => (
    <div key={repo.url}>
      <a href={repo.url}>
        {repo.author} / {repo.name}
      </a>
      <div>{repo.description}</div>
      <div>
        {repo.start} starts / {repo.forks} forks
      </div>
    </div>
  ));
}

export default App;
